#!/bin/bash

# Env
kubectl delete configmap p2-config-env-file
kubectl create configmap p2-config-env-file \
        --from-env-file=env.yaml

#deploy pv
kubectl delete -f ./pv1.yml
kubectl create -f ./pv1.yml
kubectl delete -f ./pv2.yml
kubectl create -f ./pv2.yml


#Front Deploy
kubectl delete deployment front
kubectl create -f ./deploy-front.yml

#Front Service
kubectl delete svc/front-service
kubectl create -f ./service-front.yml

#Minio pvc deploy
kubectl delete deployment minio
kubectl delete pvc minio-pv-claim
kubectl create -f ./deploy-minio.yml

#Minio Service
kubectl delete svc/minio-service
kubectl create -f ./service-minio.yml

#Redis Deploy
kubectl delete deployment redis
kubectl create -f ./deploy-redis.yml

#Redis Service
kubectl delete svc/redis-service
kubectl create -f ./service-redis.yml

#Mongo Controller,pvc
kubectl delete rc/mongo-controller
kubectl delete pvc mongo-pv-claim
kubectl create -f ./deploy-mongo.yml

#Mongo Service
kubectl delete svc/mongo-service
kubectl create -f ./service-mongo.yml

#Queue Extractor Deploy
kubectl delete deployment queue-extractor
kubectl create -f ./deploy-queue-extractor.yaml

#Queue Extractor Service
kubectl delete svc/queue-extractor
kubectl create -f ./service-queue-extractor.yaml

#Queue Parser Deploy
kubectl delete deployment queue-parser
kubectl create -f ./deploy-queue-parser.yaml

#Queue Parser Service
kubectl delete svc/queue-parser
kubectl create -f ./service-queue-parser.yaml

#Queue Archiver Deploy
kubectl delete deployment queue-archiver
kubectl create -f ./deploy-queue-archiver.yaml

#Queue Archiver Service
kubectl delete svc/queue-archiver
kubectl create -f ./service-queue-archiver.yaml


#Worker Extractor Deploy
kubectl delete deployment worker-extractor
kubectl create -f ./deploy-worker-extractor.yaml

#Worker Parser Deploy
kubectl delete deployment worker-parser
kubectl create -f ./deploy-worker-parser.yaml

#Worker Archiver Deploy
kubectl delete deployment worker-archiver
kubectl create -f ./deploy-worker-archiver.yaml

#Webapp Deploy
kubectl delete deployment webapp
kubectl create -f ./deploy-webapp.yaml

#Webapp Service
kubectl delete svc/webapp
kubectl create -f ./service-webapp.yaml




