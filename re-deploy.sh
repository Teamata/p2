#!/bin/bash

#folderpath tagName dockerhub k8sName k8sFile

# ./re-deploy.sh ./front-end p2_front teamata/p2_front front deploy-front.yml

docker build -t $2 ./$1
docker tag $2 $3
docker push $3
kubectl delete deployment $4
kubectl create -f ./deployment/$5