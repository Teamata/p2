import os
import json
import redis
import logging
import requests
from flask import Flask, jsonify, request
from flask_cors import CORS
from minio import Minio
from minio.error import ResponseError

MINIO_URI = os.getenv('MINIO_URI', "localhost")
MINIO_PORT = os.getenv('MINIO_PORT', "9000")
minioClient = Minio(f'{MINIO_URI}:{MINIO_PORT}',
                  access_key='admin',
                  secret_key='password',
                  secure=False)


# SOS_HOST = "http://"+os.getenv("SOS_HOST", "127.0.0.1")
# SOS_PORT = os.getenv("SOS_PORT", 8080)
# BASE_URL = f"{SOS_HOST}:{SOS_PORT}"
STATUS_OK = requests.codes['ok']
STATUS_BAD_REQUEST = requests.codes['bad_request']
STATUS_NOT_FOUND = requests.codes['not_found']
LOG = logging

app = Flask(__name__)
cors = CORS(app)

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    QUEUE_NAME = 'queue:zip'
    REPORT_QUEUE = 'queue:report'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

"""
    request: 
        {
         bucketName: String,
         objectName: String, //original filename.tar.gz
        }
"""
@app.route('/zip', methods=['POST'])
def zip():
    body = request.json
    json_packed = json.dumps(body)
    bucket_name, object_name = None, None
    try:
        task = json.loads(json_packed)
    except Exception:
        LOG.exception('json.loads failed')
    bucket_name = task.get('bucketName')
    object_name = task.get('objectName')
    LOG.info(f'{bucket_name},{object_name}')
    #check if these parameters exist in the json
    if not bucket_name:
        return jsonify({'status': 'error', 'error': 'bucket not found in request'}), 400
    if not object_name:
        return jsonify({'status': 'error', 'error': 'object not found in request'}), 400
    #check if the buckets, and objects exist in the data base
    data = {}
    data['isSlave'] = False
    data['name'] = "archiver"
    data['status'] = "success"
    data['bucketName'] = bucket_name
    data['objectName'] = object_name
    strip_objectname = object_name.split(".")[0]
    data['targetName'] = f"{strip_objectname}-txt.tar.gz"
    try:
        minioClient.get_object(bucket_name, object_name)
        status_packed = json.dumps(data)
        RedisResource.conn.rpush(
            RedisResource.REPORT_QUEUE,
            status_packed)
        RedisResource.conn.rpush(
            RedisResource.QUEUE_NAME,
            json_packed)
        return jsonify(status_packed),200
    except ResponseError as err:
        print(err)
        data['status'] = err
        json_packed = json.dumps(data)
        return jsonify(status_packed),400
