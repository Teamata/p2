import os
import logging
import json
import uuid
import redis
import hashlib
import sys
import tarfile
from minio import Minio
from minio.error import ResponseError
import shutil


# SOS_HOST = "http://"+os.getenv("SOS_HOST", "127.0.0.1")
# SOS_PORT = os.getenv("SOS_PORT", 8080)
# BASE_URL = f"{SOS_HOST}:{SOS_PORT}"
LOG = logging

INSTANCE_NAME = uuid.uuid4().hex

MINIO_URI = os.getenv('MINIO_URI', "localhost")
MINIO_PORT = os.getenv('MINIO_PORT', "9000")
minioClient = Minio(f'{MINIO_URI}:{MINIO_PORT}',
                  access_key='admin',
                  secret_key='password',
                  secure=False)

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    REPORT_QUEUE = 'queue:report'
    QUEUE_NAME = 'queue:extractor'
    
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task)
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                LOG.info("successfully get in worker")
                LOG.info(task)
                callback_func(task)

def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = RedisResource.REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, RedisResource.REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn, 
        RedisResource.QUEUE_NAME, 
        lambda task_descr: extractor(named_logging, task_descr))

"""
    incoming json:
    {bucketName: String,
     	objectName: String}
"""

def extractor(log, task):
    bucket_name, target_name = None, None
    bucket_name = task.get('bucketName')
    object_name = task.get('objectName')
    #create temporary directory for extracting
    temp_path = f"./{bucket_name}"
    os.makedirs(temp_path)
    resp = minioClient.fget_object(bucket_name, object_name, f"{temp_path}/{object_name}")
    with tarfile.open(f"./{bucket_name}/{object_name}") as tar:
    	tar.extractall(path=temp_path)
    # when done, make a request back to minio and upload every file except the original zip
    data = {}
    data['isSlave'] = True
    data['name'] = "extractor"
    data['status'] = "success"
    data['bucketName'] = bucket_name
    data['objectName'] = object_name
    count = 0
    try :
        for file in os.listdir(temp_path):
            if file != object_name and file.split(".")[-1] == 'pdf' and file[0] != '.':
                minioClient.fput_object(bucket_name, file , f"{temp_path}/{file}")
                count += 1
    except ResponseError as err:
        #cannot get respones from minioClient
        print(err)
        data['status'] = err
    # send new queue status to main controller 
    data['total'] = count
    json_packed = json.dumps(data)
    LOG.info(f"{RedisResource.REPORT_QUEUE}")
    LOG.info(data)
    RedisResource.conn.rpush(
            RedisResource.REPORT_QUEUE,
            json_packed)
    tar.close()
    shutil.rmtree(temp_path)
    LOG.info(f"end of uploading")
    return


if __name__ == '__main__':
    main()
	# extractor("log","task")
    # all_files = os.listdir("./example")
    # for file in os.listdir("./example"):
    #     if file != "result.tar.gz":
    #         minioClient.fput_object("example", file, f"./example/{file}")
    # # print(all_files)