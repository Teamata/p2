import sys
import os
import logging
import json
import uuid
import redis
import io
from minio import Minio
from minio.error import ResponseError
import subprocess


LOG = logging
INSTANCE_NAME = uuid.uuid4().hex

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

MINIO_URI = os.getenv('MINIO_URI', "localhost")
MINIO_PORT = os.getenv('MINIO_PORT', "9000")
minioClient = Minio(f'{MINIO_URI}:{MINIO_PORT}',
                  access_key='admin',
                  secret_key='password',
                  secure=False)

class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    QUEUE_NAME = 'queue:pdfparser'
    REPORT_QUEUE = 'queue:report'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task)
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                LOG.info("successfully get in worker")
                LOG.info(task)
                callback_func(task)

def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = RedisResource.REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, RedisResource.REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn, 
        RedisResource.QUEUE_NAME, 
        lambda task_descr: pdfparser(named_logging, task_descr))

"""
	Incoming json
	{bucketName: String,
     	objectName: String}
"""

def pdfparser(log, task):
    bucket_name, target_name = None, None
    LOG.info('working on job')
    bucket_name = task.get('bucketName')
    object_name = task.get('objectName')
    target_name = object_name.rsplit('.', 1)[0]+".txt"
    LOG.info(target_name)
    temp_path = f"./{bucket_name}"
    if (not os.path.exists(temp_path)):
        os.makedirs(temp_path)
    data = {}
    data['isSlave'] = True
    data['name'] = "parser"    
    data['status'] = "success"
    data['bucketName'] = bucket_name
    data['objectName'] = object_name
    data['targetName'] = target_name
    try:
        minioClient.fget_object(bucket_name, object_name, f"{temp_path}/{object_name}")
        LOG.info(f"pdftotext -layout {temp_path}/{object_name} {temp_path}/{target_name}")
        subprocess.run(args=["pdftotext", "-layout", f"{temp_path}/{object_name}", f"{temp_path}/{target_name}"])
        # os.system(f"pdftotext -layout {temp_path}/{object_name} {temp_path}/{target_name}")
        ##successfully create that text file 
        minioClient.fput_object(bucket_name, target_name, f"{temp_path}/{target_name}")
        ##upload to bucket
        LOG.info(f"successfully upload everything to minio")
    except ResponseError as err:
        print(err)
        data['status'] = err
    json_packed = json.dumps(data)
    LOG.info(f"{RedisResource.REPORT_QUEUE}")
    LOG.info(data)
    RedisResource.conn.rpush(
            RedisResource.REPORT_QUEUE,
            json_packed)  
    ##delete generated file and downloaded file
    if os.path.isfile(f"{temp_path}/{target_name}"):
        os.remove(f"{temp_path}/{target_name}")
    if os.path.isfile(f"{temp_path}/{object_name}"):
        os.remove(f"{temp_path}/{object_name}")
    LOG.info(f"end of parsing")
    return 


if __name__ == '__main__':
    main()
	# file_name = task.get(file_name)
    # target_name = "a2.pdf".split(".")[0]
    # os.system(f"pdftotext -layout {object_name} {target_name}.txt")