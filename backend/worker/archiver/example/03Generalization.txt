 1                                           Pattern Recognition

 2              Generalization and Cost Function
 3



 4   Intuition from Last Exercise
                                                                                                                   f = +1
 5      • Perceptron is just having a straight line and
 6        assign everything above to one. class and
 7        everything below to another class.

 8      • The math is simple h(x) = sign(wT x).                                                                    f = −1

 9        Don’t forget to pad the x.
                                                             33

                                                             34       It is impossible to say what is the value for
10      • The line is defined by the relation wT x = 0.
11        What do you expect the boundary that sep-
12        arate positive and negative, it must be zero.

13      • The learning algorithm is there to pick the
14        “best” line.
                                                             35



15      • The more training data you have the more           36       • One could say it is +1 if the lower left cor-
16        chance that your Eout will match Ein . We          37         ner is white and -1 otherwise.
17        hope that our hypothesis will “generalize”
                                                             38       • One could say it is +1 if there exist a sym-
18        Ein = Eout .
                                                             39         metry axis and -1 otherwise.

19      • The less complicated your model is the             40        Which one is right? Both perform perfectly
20        more chance that your Eout will match Ein .        41    on the training dataset. The real world problem
21        Intuitively, the less complicate your model        42    is not on the training dataset. It is how your
22        is the chance is that you will actually learn      43    hypothesis will perform out of sample
23        instead of memorizing.                             44        Is it possible for us to learn? Short answer is
                                                             45    yes. The long answer is Probably Approximately
                                                             46    yes. (This is a real mathematical term).
24      • However this doesn’t tell you that Eout will
25        be low. It just says that Ein and Eout will
26        be close. That is they can both be terrible        47    Ein, Eout, Generalization
27        but they are closely terrible..
                                                             48    To know exactly what we mean by learning, we
                                                             49    need to understand the concept of Generaliza-
                                                             50    tion. Generalization is all about how the in sam-
28   Feasibility of Learning                                 51    ple error compare to the out of sample error. We
                                                             52    define the term error quite simply as the fraction
29   Learning is trying to find out the target function.     53    of sample we got wrong. The in sample error is
30   The only information we have at the hand is the         54    calculated by taking the ratio of the number of
31   training data.                                          55    sample we got wrong class for it and the total
32       Consider the following puzzle.                      56    number of sample


     Pattern Recognition: Week 2                           1of 4                    Generalization and Cost Function
                                                                                               88    transforming (x1 , x2 ) to Z space might be a good
                                                                                               89    idea. We can do this by just using Q : X → Z:
                      # of training sample we misclassify
        Ein =                                             (1)
                          Total # of training sample
                                                                                                                                     2
57   The out of sample is defined similarly.                                                                                         x
                                                                                                                              Q(x) = 12                    (3)
                                                                                                                                     x2

                                  Eout = P (h(x) 6= f (x))                            (2)
                                                                                               90       After we transform our data from X space to
58   This Eout is an unknown quantity signifying the                                           91    Z space it is clear that we can just use linear
59   probability that the hypothesis(h) will not match                                         92    perceptron to separate the two.
60   the real target function(f ) in real world.                                               93       Your homework is to design a program that
61       It is very important that you understand the                                          94    does this (and plot it out). This is an excellent
62   different the two. Exercise 1 should give you a                                           95    programming exercise.
63   very good idea of the difference between the two.
64       When we got our hypothesis from the learn-
65   ing algorithm, we hope for two things
                                                                                               96    Noisy Target
66             • Ein is low. This means that our hypoth-
67               esis performs well at least in the training                                   97    In all of the application, your target function is
68               sample.                                                                       98    not even deterministic. The same input vector
                                                                                               99    may give you different output. For example, con-
69             • The hypothesis will generalize Ein ≈ Eout .
                                                                                              100    sider the credit card application, someone with
70               That it will continue to perform well out of
                                                                                              101    the same salary, same sex, same debt and same
71               sample.
                                                                                              102    number of dependent my have entirely different
72      Can you tell me what to do so that the chance                                         103    credit behavior.
73   that Ein and Eout match is higher?                                                       104        The target function that we talk about is
74      Remember when we do pattern recognition                                               105    really target probability (density). The target
75   we only use Ein as a guideline. Eout is the real                                         106    probability is also a conditional one: P (y|x).
76   thing we are trying to make it low.                                                      107    This reads the probability that the object of in-
                                                                                              108    put vector x will be of class y. Or concretely the
                                                                                              109    probability that a customer with x (salary, debt)
77   Non Linear Transformation                                                                110    would be good customer(y=+1).
                                                                                              111        An example of noisy target function is shown
78   Enough for the theory, now let us go back to
                                                                                              112    below
79   practical stuff.
80       Not all target function is linearly separa-
                                                                                                             5
81   ble. But we can sort of making it more linearly
82   separable by doing a non-linear transformation.                                                         4
      1.0                                             0.9

                                                      0.8
                                                                                                             3
                                                      0.7
      0.5
                                                      0.6

                                          X →Z        0.5
                                                                                                             2
      0.0
                                                      0.4

                                                      0.3                                                    1
      0.5
                                                      0.2

                                                      0.1

      1.0                                             0.0
                                                                                                             0
         1.0    0.5   0.0   0.5     1.0                 0.0   0.2   0.4   0.6   0.8    1.0
83
                                                                                                             1
84       For example, in the figure above, the X space
85   on the left is not linearly separable. But, looking                                                     2
86   at it, we can see that ok the classes has some-
                                                                                                             3
                                                                                                                 3   2   1     0   1   2   3   4   5   6
87   thing to do with the distance from teh origin. So,                                       113




     Pattern Recognition: Week 2                                                             2of 4                           Generalization and Cost Function
114   Pocket Algorithm                                       155          this is quite a big deal since you will em-
                                                             156          barass a loyal customer. We really do not
115   So far we have learn Perceptron Learning Algo-         157          want this to happen. So we would penalize
116   rithm which deals with linearly separable data.        158          this by a lot.
117       If we apply Perceptron Learning Algorithm to
118   non-linearly separable data. The algorithm will        159      4. x is not in the database and the fingerprint
119   not stop. (Why?)                                       160         algorithm rejects x. This is good. It does
120       We can modify this very simply by just keep-       161         what it is supposed to do.
121   ing(pocketing) the one with the best error rate
                                                             162        Given the above requirement that we really
122   as we go. Then we stop after number of iteration
                                                             163    don’t want false negative. Intuitively, if you make
123   and return the best one we found so far.
                                                             164    your algorithm very strict, you will get less false
124       See exercise.
                                                             165    positive but you will get more false positive vice
                                                             166    versa. It is clear from the requirement that the
125   Cost Function                                          167    false negative and false positive should not be
                                                             168    penalized as equal. The false negative should be
126   In pocket algorithm, we choose the hypothesis          169    penalize a lot more. So once could construct a
127   with the lowest Ein defined as the fraction of         170    cost function that looks like the following:
128   training data we misclassify.                          171

129       Of course, the final hypothesis must have                                        Accept Reject
130   something special. But, you may ask is this Ein        172         In the DB           0     1000
131   really the thing we want to minimize?                  173
                                                                        Not in the DB        10      0
132       The true answer is depends on the applica-         174    This table can be translated to the the formula
133   tion.
134       Let us consider the a fingerprint algorithm.
                                                                                   1
135       It takes your fingerprints and it tells whether                 E(h) =     (1000 × # of false negative
136   the customer is in the database or not.                                      N
137       There are two customers for this a supermar-                             + 10 × # of false positive)
138   ket and CIA.
                                                             175       If we minimize this cost function, will will get
                                                             176    ≈ the thing we want.
139   Supermarket                                            177       Note that given a fixed training data, the cost
                                                             178    function is a function of hypothesis. Each hy-
140   The supermarket use fingerprint identification to
                                                             179    pothesis will give you different amount of false
141   give discount to loyal customer. There are 4
                                                             180    negative an false postive.
142   things that can happen.

143     1. A customer x is in the database and the           181    CIA
144        fingerprint algorithm say that x is in the
145        database.                                         182    Now let us consider the same fingerprint applica-
                                                             183    tion for CIA agents.
146     2. x is not in the database but the finger-          184        When the fingerprint does the right thing.
147        print says that [ x is in the database. This      185    Good, we like it. But, the real deal is when it
148        situation is called false positive. But such      186    misclassify. There are two ways to misclassify:
149        situation is not so bad for the supermar-
150        ket. They just have to give a discount to         187      1. False negative. x is a CIA agents. He tries
151        another customer. No big deal.                    188         to log into his computer using fingerprint.
                                                             189         The algorithm denies him. No big deal. He
152     3. x is in the database but the fingerprint          190         will just try again and if after 10th try it
153        says that x is not in the database. This          191         doesn’t work he can just go ask IT dept to
154        is called false negative. For supermarket,        192         fix it.


      Pattern Recognition: Week 2                           3of 4                     Generalization and Cost Function
193     2. False positive. x is not a secret CIA agent      204       which translate to
194        but a spy. If the system let the spy in, tons
195        of information will leak and that is super
                                                                                 1
196        bad for the CIA.                                             E(h) =     (1 × # of false negative
                                                                                 N
                                                                                 + 1000 × # of false positive)
197      Given the above requirement, we really
198   should want to penalize false negative heavily.
199   One may come up with a cost function that             205    Take Home Lesson
200   looks something like:                                 206    Cost function is something to minimize to get
201
                                                            207    what you want so make one up to do what you
                        Accept Reject
                                                            208    need to do.
202        In the DB      0      1
                                                            209       Next week we are going to learning to tell
203
          Not in the DB  1000    0                          210    computer to minimize the cost function.




      Pattern Recognition: Week 2                          4of 4                    Generalization and Cost Function
