import os
import logging
import json
import uuid
import redis
import hashlib
import sys
import tarfile
from minio import Minio
from minio.error import ResponseError
import shutil

MINIO_URI = os.getenv('MINIO_URI', "localhost")
MINIO_PORT = os.getenv('MINIO_PORT', "9000")
minioClient = Minio(f'{MINIO_URI}:{MINIO_PORT}',
                  access_key='admin',
                  secret_key='password',
                  secure=False)

LOG = logging

INSTANCE_NAME = uuid.uuid4().hex

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    REPORT_QUEUE = 'queue:report'
    QUEUE_NAME = 'queue:zip'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task)
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                LOG.info("successfully get in worker")
                LOG.info(task)
                callback_func(task)

def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = RedisResource.REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, RedisResource.REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn, 
        RedisResource.QUEUE_NAME, 
        lambda task_descr: archiver(named_logging, task_descr))

"""
    incoming json:
    {bucketName: String,
     	object_name: String,
    	t: String}
"""

def archiver(log, task):
    bucket_name, target_name = None, None
    bucket_name = task.get('bucketName')
    object_name = task.get('objectName')
    strip_objectname = object_name.split(".")[0]
    LOG.info(strip_objectname)
    temp_path = f"./{bucket_name}"
    data = {}
    data['isSlave'] = True
    data['name'] = "archiver"
    data['status'] = "success"
    data['bucketName'] = bucket_name
    data['objectName'] = object_name
    data['targetName'] = f"{strip_objectname}-txt.tar.gz"
    if (not os.path.exists(temp_path)):
        os.makedirs(temp_path)
    try:
        with tarfile.open(f"{temp_path}/{strip_objectname}-txt.tar.gz", "w:gz") as tar:
            objects_list = minioClient.list_objects(bucket_name)
            for obj in objects_list:
                if (obj.object_name.split(".")[-1] == 'txt') and (obj.object_name[0] != '.'):
                    minioClient.fget_object(bucket_name, obj.object_name, f"{temp_path}/{obj.object_name}")
                    tar.add(f"{temp_path}/{obj.object_name}", arcname=os.path.basename(f"{temp_path}/{obj.object_name}"))
                    LOG.info(f"{temp_path}/{obj.object_name}")
        minioClient.fput_object(bucket_name, f"{strip_objectname}-txt.tar.gz" , f"{temp_path}/{strip_objectname}-txt.tar.gz")
    except ResponseError as err:
        LOG.info(err)
        data['status'] = "failed"
    json_packed = json.dumps(data)
    RedisResource.conn.rpush(
            RedisResource.REPORT_QUEUE,
            json_packed)  
    os.remove(f"{temp_path}/{strip_objectname}-txt.tar.gz")
    return


if __name__ == '__main__':
    main()
