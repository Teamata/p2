#!/usr/bin/env python3
import os
import logging
import json
import uuid
import redis
import requests
import hashlib
import sys
import tarfile
from minio import Minio
from minio.error import ResponseError
import shutil


# SOS_HOST = "http://"+os.getenv("SOS_HOST", "127.0.0.1")
# SOS_PORT = os.getenv("SOS_PORT", 8080)
# BASE_URL = f"{SOS_HOST}:{SOS_PORT}"
STATUS_OK = requests.codes['ok']
STATUS_BAD_REQUEST = requests.codes['bad_request']
STATUS_NOT_FOUND = requests.codes['not_found']
LOG = logging
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
REPORT_QUEUE = 'queue:report'

INSTANCE_NAME = uuid.uuid4().hex

minioClient = Minio('localhost:9000',
                  access_key='admin',
                  secret_key='password',
                  secure=False)

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    QUEUE_NAME = 'queue:report'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task)
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                LOG.info("successfully get in worker")
                LOG.info(task)
                callback_func(task)

def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn, 
        REPORT_QUEUE, 
        lambda task_descr: status_report(named_logging, task_descr))

"""
    incoming json:
    {bucketName: String,
        objectName: String}
"""

def status_report(log, task):
    slave, status, bucket_name, target_name = None, None, None, None
    bucket_name = task.get('bucketName')
    object_name = task.get('objectName')
    slave = task.get('slave')
    status = task.get('status')
    LOG.info(f"slave: {slave}")
    LOG.info(f"status: {status}")
    LOG.info(f"bucketName: {bucket_name}")
    LOG.info(f"objectName: {object_name}")
    return


if __name__ == '__main__':
    main()