import os
import logging
import json
import uuid
import redis
import requests
import hashlib
import sys
from flask import Flask, jsonify, request, send_file
from flask_cors import CORS
from minio import Minio
from minio.error import ResponseError
import shutil
from pymongo import MongoClient
from flask_socketio import SocketIO, join_room, leave_room
import threading
import time

app=Flask(__name__)
socket_io = SocketIO(app, async_mode = 'threading')


cors= CORS(app)

STATUS_OK = requests.codes['ok']
STATUS_BAD_REQUEST = requests.codes['bad_request']
STATUS_NOT_FOUND = requests.codes['not_found']
INSTANCE_NAME = uuid.uuid4().hex

EXTRACT_HOST = "http://"+os.getenv("EXTRACT_HOST", "127.0.0.1")
EXTRACT_PORT = os.getenv("QUEUE_PORT", 5001)

PARSER_HOST = "http://"+os.getenv("PARSER_HOST", "127.0.0.1")
PARSER_PORT = os.getenv("PARSER_PORT", 5002)

ZIP_HOST = "http://"+os.getenv("ZIP_HOST", "127.0.0.1")
ZIP_PORT = os.getenv("ZIP_PORT", 5003)

EXTRACT_URL = f"{EXTRACT_HOST}:{EXTRACT_PORT}"
PARSER_URL = f"{PARSER_HOST}:{PARSER_PORT}"
ZIP_URL = f"{ZIP_HOST}:{ZIP_PORT}"


REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
REPORT_QUEUE = 'queue:report'


LOG = logging

MONGO_URI = os.getenv('MONGO_URI', "localhost")
MONGO_PORT = os.getenv('MONGO_PORT', "27017")
client = MongoClient(f'mongodb://{MONGO_URI}:{MONGO_PORT}')

MINIO_URI = os.getenv('MINIO_URI', "localhost")
MINIO_PORT = os.getenv('MINIO_PORT', "9000")
minioClient = Minio(f'{MINIO_URI}:{MINIO_PORT}',
          access_key='admin',
          secret_key='password',
          secure=False)


mongo = client.mongo
db = mongo.db.p2

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)


REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
REPORT_QUEUE = 'queue:report'

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task)
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)

def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn,
        REPORT_QUEUE,
        lambda task_descr: status_report(named_logging, task_descr))

@app.route('/<bucket_name>')
def download(bucket_name):
    LOG.info(f"----*|| b: {bucket_name} ||*----")
    try:
        mydata = db.find_one({"bucket_name" : bucket_name})
    except:
        LOG.info("weird things happen in mongo")
        return jsonify({"status" : "error", "error" : "unknown error in mongo"}), 400
    if mydata is None:
        LOG.info("cannot find this bucket")
        return jsonify({"status" : "error", "error" : "bucket name is invalid"}), 400
    strip_objectname = mydata["object_name"].split(".")[0]
    temp_path = f"./{bucket_name}"
    parsed_obj = f"{strip_objectname}-txt.tar.gz"
    if (not os.path.exists(temp_path)):
        os.makedirs(temp_path)
    minioClient.fget_object(bucket_name, parsed_obj, f"{temp_path}/{parsed_obj}")
    return send_file(f"{temp_path}/{parsed_obj}",
                     mimetype='application/gzip',
                     attachment_filename=parsed_obj,
                     as_attachment=True)


@app.route('/validation/<bucket_name>')
def validate(bucket_name):
    LOG.info(f"----*|| b: {bucket_name} ||*----")
    try:
        mydata = db.find_one({"bucket_name" : bucket_name})
    except:
        LOG.info("weird things happen in mongo")
        return jsonify({"status" : "error", "error" : "unknown error in mongo"}), 200
    if mydata is None:
        LOG.info("cannot find this bucket")
        return jsonify({"status" : "error", "error" : "bucket name is invalid"}), 200
    mydata['_id'] = ''
    return jsonify(mydata), 200

"""
incoming json:
{bucketName: String,
objectName: String}
"""
@socket_io.on('init')
def initiate(data):
    # body = request.json
    json_packed = json.dumps(data)
    bucket_name, object_name = None,None
    try:
        task = json.loads(json_packed)
    except Exception:
        LOG.exception('json.loads failed')
    bucket_name = task.get('bucketName')
    object_name = task.get('objectName')
    LOG.info(f'{bucket_name},{object_name}')
    if not bucket_name:
        socket_io.send(data={"name":"", "status": "error", "total": 0, "current": 0, "message": "Cant get bucket_name from body"}, room=bucket_name, json=True)
        # socket_io.emit('front receive', data={"name":"", "status": "error", "total": 0, "current": 0, "message": "Cant get bucket_name from body"}, room= bucket_name)
    if not object_name:
        socket_io.send(data={"name":"", "status": "error", "total": 0, "current": 0, "message": "Cant get object_name from body"}, room=bucket_name, json=True)
        # socket_io.emit('front receive', data={"name":"", "status": "error", "total": 0, "current": 0, "message": "Cant get object_name from body"}, room= bucket_name)
    try:
        minioClient.bucket_exists(bucket_name)
    except ResponseError as err:
        socket_io.send(data={"name":"", "status": "error", "total": 0, "current": 0, "message": "ResponseError line 193"}, room=bucket_name, json=True)
        # socket_io.emit('front receive', data={"name":"", "status": "error", "total": 0, "current": 0, "message": "ResponseError line 193"}, room= bucket_name)
        return
    mydata = { "name" : "queue", "status" : "success", "bucket_name": bucket_name, "object_name": object_name, "total" : 0, "current" : 0}
    db.insert_one(mydata)
    mydata['_id'] = ''
    resp = requests.post(f'{EXTRACT_URL}/extract', json = task)
    if resp.status_code == STATUS_OK:
        socket_io.send(data=mydata, room=bucket_name, json=True)
        # socket_io.emit('front receive', data=mydata, room= bucket_name)
    else :
        socket_io.send(data={"name":"", "status": "error", "total": 0, "current": 0, "message":"STATUS_NOT_OK"}, room=bucket_name, json=True)
        # socket_io.emit('front receive', data={"name":"", "status": "error", "total": 0, "current": 0, "message":"STATUS_NOT_OK"}, room= bucket_name)


def status_report(log, task):
    print("IN Status Report")
    status = task.get('status')
    report_name = task.get('name')
    bucket_name = task.get('bucketName')
    object_name = task.get('objectName')
    is_slave = task.get('isSlave')
    mydata = db.find_one({"bucket_name" : bucket_name})
    passing_data = {"bucketName" : bucket_name, "objectName" : mydata['object_name']}
    LOG.info(f"task : {task}")
    if status != "success":
        mydata["status"] = "error"
        #throws to frontend that bad shit happen
    if is_slave == True:
    #worker report
        if report_name == "extractor":
            #successfully extract file
            mydata["total"] = task.get('total')
            mydata["name"] = "parser"
            mydata["status"] = "working"
            resp = requests.post(f'{PARSER_URL}/parse', json = passing_data)
        elif report_name == "parser":
            mydata["current"] = mydata["current"]+1
            if mydata["current"] == mydata["total"]:
                mydata["name"] = "archiver"
                mydata["status"] = "success"
                resp = requests.post(f'{ZIP_URL}/zip', json = passing_data)
        else:
            mydata["status"] = "success"
    else:
    #controller report
        if report_name == "extractor":
            mydata["name"] = "extractor"
            mydata["status"] = "working"
        elif report_name == "parser":
            mydata["name"] = "parser"
            mydata["status"] = "working"
        else:
            mydata["name"] = "archiver"
            mydata["status"] = "working"
        #controller report
    db.save(mydata)
    mydata['_id'] = ''
    # time.sleep(5)
    socket_io.send(mydata, room=bucket_name, json=True)

def watch_report_queue():
    main()

def server():
    app = Flask(__name__)
    app = socketio.Middleware(socket_io, app)
    eventlet.wsgi.server(eventlet.listen(('', 5000)), app)

@socket_io.on('join')
def on_join(data):
    LOG.info("joined connection")
    LOG.info("joined connection")
    LOG.info("joined connection")
    LOG.info("joined connection")
    # socket_io.send(data=mydata, room=bucket_name, json=True)
    join_room(data)
    socket_io.send(mydata, room=bucket_name, json=True)


@socket_io.on('leave')
def on_leave(data):
    LOG.info("leaving")
    leave_room(data)


if __name__ == '__main__':
    thread = socket_io.start_background_task(target = main)

    LOG.info("starting server... accepting connection")
    socket_io.run(app, host="0.0.0.0", port=5000)
