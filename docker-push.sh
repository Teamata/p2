#!/bin/bash

docker tag p2_web-app teamata/p2_web-app 
docker push teamata/p2_web-app 

docker tag p2_front teamata/p2_front 
docker push teamata/p2_front 

docker tag p2_worker-parser teamata/p2_worker-parser
docker push teamata/p2_worker-parser

docker tag p2_worker-archiver teamata/p2_worker-archiver
docker push teamata/p2_worker-archiver

docker tag p2_worker-extractor teamata/p2_worker-extractor
docker push teamata/p2_worker-extractor

docker tag p2_queue-parser teamata/p2_queue-parser
docker push teamata/p2_queue-parser

docker tag p2_queue-extractor teamata/p2_queue-extractor
docker push teamata/p2_queue-extractor

docker tag p2_queue-archiver teamata/p2_queue-archiver
docker push teamata/p2_queue-archiver