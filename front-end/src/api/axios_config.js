import Axios from 'axios';
import QS from 'qs';

const webAppURL = process.env.VUE_APP_API || 'http://pdf-to-text.teamata.me:30005/';
const minioURL = process.env.VUE_APP_API || 'http://pdf-to-text.teamata.me:9000/';


const axios = Axios.create({
  baseURL: webAppURL,
  // minio: minioURL,
  // withCredentials: true,
  // headers: {
  //   'Content-Type': 'application/x-www-form-urlencoded',
  // },
  // transformRequest: data => QS.stringify(data),
});

export default axios;