// import Minio from 'minio'

var Minio = require('minio');

var minio = new Minio.Client({
  endPoint: 'pdf-to-text.teamata.me',
  port: 30009,
  useSSL: false,
  accessKey: 'admin',
  secretKey: 'password'
});


export default minio;