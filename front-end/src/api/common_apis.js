import axios from './axios_config';
import minio from './minio_config';
import store from '@/store'


// export function getStuff() {
//   return axios.get('https://api.coindesk.com/v1/bpi/currentprice.json');
// }

//Take in a single File object
export async function upload(bucketName,file) {

  minio.bucketExists(bucketName, function(err, exists) {
    if (err) {
      console.log(err);
      return console.log(err);
    }
    if (exists){
      store.commit('changeStatus','error')
    }else{
      minio.makeBucket(bucketName, function(err) {
        if (err){
          console.log('Error creating bucket.', err)
          store.commit('changeStatus','error')
          return
        }

        console.log('Bucket created successfully in "us-east-1" as : ', bucketName)


        const name = file.name;
        const size = file.size;
        const metaData = {
          'Content-Type': "application/octet-stream",
        };

        const reader = new FileReader();

        reader.onerror = function(err) {
          store.commit('changeStatus','error')
          console.log(err)
          return
        };

        reader.onloadstart = function(status) {
          // console.log("Loading ",status);
        };

        reader.onloadend = (function(evt) {
          console.log(evt)
          if (evt.target.readyState === FileReader.DONE) {
            // console.log("DONE")
          }else{
            // console.log("SSSS")
          }
          // console.log("Load Ended")

          const buffer = new Buffer(evt.target.result)

          minio.putObject(bucketName,name ,buffer , function(err, etag)
          {
            // console.log("in minio")
            if (err){
              store.commit('changeStatus','error')
              console.log(err)
            }
            // console.log("etag " , etag)
            console.log('File uploaded successfully.');
            store.commit('changeStatus','readyToConvert')

          });
        })

        reader.readAsArrayBuffer(file)

        // console.log(reader.readyState)

      })
    }
  })
}

// register this room (bucketname) to webapp
// data = {
//    bucketName: ".."
//    objectName: "..."
// }

// export async function downloadApi(bucketName){
//
//   const path = '/'+bucketName;
//
//   // return spring.post(path, body, config);
//   axios.get(path)
// }

export async function checkID(data){

  const path = '/validation/'+data;

  // return spring.post(path, body, config);
  return axios.get(path)
}

export async function initWebApp(data){

  const path = '/init';
  const body = data;

  // return spring.post(path, body, config);
  console.log("IM HEREEEE")
  return axios.post(path,body)
}
