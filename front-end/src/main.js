import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Buefy from 'buefy'
import ShortId from 'shortid'
import 'buefy/dist/buefy.css'
// import axios from '@/api/axios_config.js'

Vue.use(Buefy, { defaultIconPack: 'fa' })
Vue.config.productionTip = false

// Vue.$axios = axios;
// Vue.$shortId = ShortId
// Vue.$shortId.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-');



new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
